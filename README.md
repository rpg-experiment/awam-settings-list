# Setting pour AWAM

Juste un petit rassemblement de settings pour le jeu => https://rpg-experiment.gitlab.io/awam-settings-list/settings/

## Pour contribuer

### Ajouter / modifier une fiche
Soit tu crées une "issue" par le [formulaire adéquat](https://gitlab.com/rpg-experiment/awam-settings-list/-/issues) en donnant le liens et toutes les explications que tu trouves habituellement sur une page.

Soit tu crées une [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) avec la nouvelle page ;).

Merci

### Le site est moche / pas pratique / ...

Si tu sais coder, faire un thème, ... tu peux te renseigner sur [Zola](https://www.getzola.org/) et son système de [template (tera)](https://tera.netlify.app/docs/).

Les contraines sont:
* Licences open-source sur le travail et outils utilisé
* Moteur de recherche intégré
* Facile a maintenir
* Facile à lire

### Pour regénérer le site localement

* Il te faut [Zola](https://github.com/getzola/zola/releases/tag/v0.17.2).
* Ensuite il faut lancer la commande : `./zola serve`
