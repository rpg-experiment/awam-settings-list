+++
title = "AWAM settings - À propos"
date = 2023-01-28
+++

## Tools & dependency

* [zola](https://www.getzola.org/) comme outil de construction de site web statique
	* [tera](https://tera.netlify.app/docs/) pour le système de template
* [Zolma](https://github.com/Worble/Zulma) le thème du site web utilisé
* [Gitlab](https://gitlab.com/) la gestion du code source et de l'hébergement

## Hack it

Tu veux corrigé l'ortougrapheu ou la grand-mere ? Ajouter un settings? autre chose

==> [https://gitlab.com/rpg-experiment/awam-settings-list](https://gitlab.com/rpg-experiment/awam-settings-list) est l'endroit où faire tout cela (ou le notifier)
