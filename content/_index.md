+++
title = "Settings pour AWAM"
sort_by = "title"
template = "index.html"
page_template = "page.html"
+++

Ce site reprend, entre-autre, [divers settings](./settings) pour le jeu de rôle (JDR) [Anime Was A Mistake (AWAM)](https://www.gameontabletop.com/mp.html?search=anime+was+a+mistake&order=desc&sort=date&tag_id=0&mp=0&language_id=0&country_id=0#) des [éditions 6napse](http://editions-6napse.fr/).

L'ensemble des [settings listé ici](./settings) ont été créer par leurs auteurs respectifs en s'inspirant d'univers existant ou non. Dans certains cas, cela nécessitera un accès au discord de 6napse. 
