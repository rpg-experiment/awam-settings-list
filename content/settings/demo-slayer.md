+++
title = "Demon Slayer"
date = 2023-05-23
updated = 2023-05-23

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1110580474029555782", author = "Akibi90", link = "https://drive.google.com/drive/folders/19_p-NPQoam0dpwrbZKtKqPuJwJeDERyP?usp=sharing", title = "Demon Slayer", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["Demon Slayer", "scenario"]
authors = ["Akibi90"]

+++

Petit dossier.

Il y a dedans le setting "Demon Slayer" et le scénario (avec ses ressources) que j'utilise pour faire découvrir AWAM (il n'est pas parfait, mais fait bien le taff).

J'apporte des modifs de temps en temps pour le peaufiner!
