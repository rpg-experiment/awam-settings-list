+++
title = "Coin Road"
date = 2022-10-24
updated = 2022-10-24

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1034152616051101807", author = "Pierre", link = "https://docs.google.com/document/d/1TUrZx4xyMA1sWfCl9c7gXewpYCOQEpJPX_TXKziTbiM/edit?usp=sharing", title = "Coin Road", status = "WIP" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Coin Road a été pensé pour émuler l’univers de One Piece écrit par Eiichiro ODA et disponible en France aux éditions Glénat.
