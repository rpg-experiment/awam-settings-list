+++
title = "Guerriers du Cosmos"
date = 2020-06-05
updated = 2020-06-05

[extra]
awam = {author = "Quentin Forestier", link = "https://quentinforestier.itch.io/guerriers-du-cosmos", title = "Guerriers du Cosmos", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["saint seiya"]
authors = ["Quentin Forestier"]

+++

Setting basé sur une ancienne version des règles pour les Chevalier du Zodiac.

A noté, que ce setting a été créer avant la sortie des règles officiel de AWAM, dans le cadre d'une jam sur els chevalier du zodias, et est jouable en lui-même mais sera probablement moins fun.
