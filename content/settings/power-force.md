+++
title = "Power Force"
date = 2024-10-19
updated = 2024-11-03

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1297184043049160796", author = "Tenkuro", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1299482410110554284/Power_Force_v1.5.pdf?ex=6728918d&is=6727400d&hm=636d07b45ddfdd5d9c96b6c5886a46be35a2b5ae877df04bc8e039b2ac4a0a12&", title = "Power Force", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["Power Rangers"]
authors = ["Tenkuro"]

+++

Power Force a été pensé pour émuler l’univers de la licence Power Ranger créée par Haim Saban et produite actuellement par Hasbro.
