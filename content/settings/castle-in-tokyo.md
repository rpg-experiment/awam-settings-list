+++
title = "Castle in Tokyo 🧛"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032697940969533514", author = "Eolias", link = "https://drive.google.com/file/d/16FKeVueapUjDBUDjHdiMxc8O-skKFVKd/view?usp=sharing", title = "Castle in Tokyo 🧛", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Eolias"]

+++

Castle in Tokyo a été pensé pour émuler l’univers de la saga Castlevania développée et éditée par Konami.

* [Couverture](https://drive.google.com/file/d/18mhcSurfevew56DqAM4GDIA-rZq6vhmt/view)
* [Livre](https://drive.google.com/file/d/16FKeVueapUjDBUDjHdiMxc8O-skKFVKd/view)
* [Fiche de perso](https://drive.google.com/file/d/1YPrvK5sqGtvCs9Cm6Pk6UBUfFiCKm_3P/view)
