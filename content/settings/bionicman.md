+++
title = "Bionicman"
date = 2022-10-23
updated = 2022-10-23

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1033759499338666054", author = "thau", link = "https://docs.google.com/document/d/1dh7kz6ZtD-5AdB-k0cnhrWrBrQ2fojsAy69Wwqsoy8E/edit?usp=sharing", title = "Bionicman", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["thau"]

+++

Ce setting est basé sur l’univers créé par Lego, Bionicle.
