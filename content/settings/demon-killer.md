+++
title = "Demon Killer"
date = 2022-10-23
updated = 2022-10-23

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1033731944623640596", author = "thau", link = "https://drive.google.com/file/d/16FKeVueapUjDBUDjHdiMxc8O-skKFVKd/view?usp=sharing", title = "Demon Killer", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["thau"]

+++

Demon killer a été pensé pour émuler l’univers de Demon Slayer (Kimetsu no yaiba) écrit par Koyoharu Gotōge éditée par Panini Comics.
