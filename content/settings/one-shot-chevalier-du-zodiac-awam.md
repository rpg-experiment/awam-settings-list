+++
title = "One Shot - Chevaliers du Zodiaque"
date = 2022-10-07
updated = 2022-10-07

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/586905829412700168/1028063378859638834", author = "Lanoar", link = "https://cdn.discordapp.com/attachments/586905829412700168/1028063378532483082/AWAM-chevaliers-du-zodiaque.pdf", title = "One Shot - Chevaliers du Zodiaque", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["saint seiya"]
authors = ["Lanoar"]

+++

En avant, en avant les Chevaliers !

Dans une époque indéterminée après les événements des séries animées concernant les aventures du groupe de Seiya. Les haut-faits des cinq chevaliers de bronze légendaires sont encore vivaces. Les personnages sont de fidèles chevaliers d’Athéna qui ont fraichement reçus leurs armures. Leur poids historique est lourd de sens et les porter est un grand honneur.

Athéna les envoie en Irak pour tirer au clair la disparition mystérieuse de la déesse Déméter. C’est leur première vraie mission ensemble.

Scénario à la fin de ce document : https://quentinforestier.itch.io/guerriers-ducosmos

[Feuille de perso](https://cdn.discordapp.com/attachments/586905829412700168/1028383233961885717/AWAM_FDP_A4_Couleur_2022_1.pdf)
