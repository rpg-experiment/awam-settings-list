+++
title = "Tribal War Survival"
date = 2022-11-01
updated = 2022-11-01

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1036910325708099584", author = "Pierre", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1036910764746887248/Tribal_War_Survivor.docx", title = "Tribal War Survival – Buzoku Sensô No Seizon V.3.0", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Bienvenue dans Tribal War Survival – Buzoku Sensô No Seizon, chef de guerre ou sage chaman sera tu suffisamment douer pour faire prospérer et survire ta tribu ?

Vous pouvez faire jouer ce setting dans un cadre plus cool avec une ambiance fun et détendu dans un univers un peu loufoque style donjon de Naheulbeuk , Konosuba ou bien plus dark comme gobelin slayer ou encore Berserk.
