+++
title = "Jojo's strange stories"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032687332639379476", author = "Pierre", link = "https://docs.google.com/document/d/1F-82QUMFTYjxgJQJgyczR1lfEel-0EKFh14WVjjPiEk/edit?usp=sharing", title = "Jojo’s Strange Storie", status = "WIP" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Jojo’s Strange Storie a été pensé pour émuler l’univers du manga Jojo’s Bizarre Adventure écrit par Hirohiko Araki et publié en France aux éditions Tonkam.
