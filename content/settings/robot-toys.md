+++
title = "Robot toys 🤖 🇯🇵"
date = 2022-11-05
updated = 2022-11-05

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1038399129283469322", author = "Eolias", link = "https://drive.google.com/file/d/10T3dg3s0xTWCWgeueZh9jYrR8Vr5fzeg/view?usp=share_link", title = "Robot toys 🤖 🇯🇵", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Eolias"]

+++

Robot toys a été pensé pour émuler l’univers de la saga Medabots développée par Natsume et éditée par Imagineer.

* [Livre](https://drive.google.com/file/d/10T3dg3s0xTWCWgeueZh9jYrR8Vr5fzeg/view?usp=share_link)
* [Feuille de perso - Medabot](https://drive.google.com/file/d/1QPWV63glhMJCYereAJOCevhzQU8z9hnY/view?usp=share_link)
* [Feuille de perso - Medachampion](https://drive.google.com/file/d/1o-nn35QGsahrIT5Gyv391Zg1AFDuCsh_/view?usp=share_link)
