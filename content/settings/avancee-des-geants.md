+++
title = "L'Avancée des Géants"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032687226510901278", author = "Pierre", link = "https://docs.google.com/document/d/1SKCKFXB5GeNwIEHJDvxGi8JVR-L22-qHPGMTZ035KQA/edit?usp=sharing", title = "L'Avancée des Géants", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

L’Avancée des Géants a été pensé pour émuler l’univers de L’Attaque des Titans (Shingeki no Kyojin) écrit par Hajime Isayama et disponible en France aux éditions PIKA.
