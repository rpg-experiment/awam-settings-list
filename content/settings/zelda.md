+++
title = "Zelda"
date = 2024-02-04
updated = 2024-11-03

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1203699098923044864", author = "Llenard", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1203699098558013470/Zelda_-_JDR_-_AWAM.pdf?ex=6728d124&is=67277fa4&hm=7c82e65fad5615814870e4e046433274b0d6659267a88f21378f91aae57f6ae7&", title = "Roads of Anger", status = "Beta" }

[taxonomies]
categories = ["settings"]
tags = ["zelda", "jeux vidéos"]
authors = ["Llenard"]

+++

Bienvenue, aventurier des terres d’Hyrule et d’ailleurs !

Le texte que vous lisez est une adaptation des différents jeux de la saga Zelda motorisé par le système d’Anime Was A Mistake.

Ce livret vous permettra de vivre des aventures riches en action et en émotion au cœur d’Hyrule, d’explorer des donjons anciens ou des cités volantes ! Incarnez un héros d’une des nombreuses races de cet univers et affrontez les forces des ténèbres dans des confrontations épiques où se joueront l’avenir d’Hyrule !
