+++
title = "World of Remnant AWAM"
date = 2022-10-23
updated = 2022-10-23

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1033866219763290143", author = "Spice", link = "https://docs.google.com/document/d/1xu8NOCTPFLF7DH77XslYAFncVYyXuCxBGLmUFuWQwqo/edit?usp=sharing", title = "World of Remnant AWAM", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Spice"]

+++

World of Remnant a été pensé pour émuler l’univers de RWBY produit par Rooster Teeth.
