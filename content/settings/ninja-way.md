+++
title = "Ninja's way"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032687267430543381", author = "Pierre", link = "https://docs.google.com/document/d/1yNvNTu2Mo13C1MRE2-mNQtUbAIB5sO4HD8WeHflRAdM/edit?usp=sharing", title = "Ninja's way", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Ninja’s Way a été pensé pour émuler l’univers de Naruto écrit par Masashi Kishimoto et disponible en France aux éditions KANA.
