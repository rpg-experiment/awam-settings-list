+++
title = "Bento AzurLane"
date = 2023-09-01
updated = 2023-09-01

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1147138913543131198", author = "itche", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1147138913417297930/bento_AzurLane_pitche.pdf", title = "Bento AzurLane", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["pitche"]
tags = ["Azur Lane"]

+++

Azur Lane est univers inspiré du jeu vidéo et de la série animée Azur Lane.
"Car jamais la guerre ne cessera."
