+++
title = "Blade Skill Online"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032687368647491674", author = "Pierre", link = "https://docs.google.com/document/d/1w56zKSfQhHfWTvMXfzy3JL14xFjqqXUjTEBPDz78A_g/edit?usp=sharing", title = "Blade Skill Online", status = "WIP" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Blade Skill Online (BSO) a été pensé pour émuler l’univers de Sword Art Online écrit par Reki Kawahara et publié en France aux éditions Ototo.
