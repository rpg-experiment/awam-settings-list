+++
title = "Space Mecha Force 🤖 ⭐"
date = 2022-11-05
updated = 2022-11-05

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1043917397758853242", author = "Eolias", link = "https://drive.google.com/file/d/1PWXAw621pGAls7JRgkkTvk-fD0uprHM0/view?usp=share_link", title = "Space Mecha Force 🤖 ⭐", status = "WIP" }

[taxonomies]
categories = ["settings"]
authors = ["Eolias"]

+++

Space Mecha Force a été pensé pour émuler de nombreux univers dans lesquels des pilotes de mecha se battent pour la liberté, la justice et la paix.
