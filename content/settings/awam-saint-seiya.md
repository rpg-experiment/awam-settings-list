+++
title = "Saint Seiya settings AWAM"
date = 2023-02-01
updated = 2023-02-01

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1070252240775938078", author = "oursgeek", link = "https://docs.google.com/document/d/1RYyJwEkgW0H27DwMSU_ZeMw4jFwmjiTnBfFCp4qwy2g/edit?usp=sharing", title = "Saint Seiya settings AWAM", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["saint seiya"]
authors = ["oursgeek"]

+++

Ce bento a été pensé pour émuler l’univers de Saint Seiya.
