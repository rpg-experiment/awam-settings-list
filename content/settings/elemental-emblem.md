+++
title = "Elemental Emblem"
date = 2022-11-05
updated = 2022-11-05

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/586905829412700168/1015235991193272400", author = "Eolias", link = "https://cdn.discordapp.com/attachments/586905829412700168/1015235990962577408/Elemental_Emblem.pdf", title = "Elemental Emblem", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Eolias"]

+++

Elemental Emblem a été pensé pour émuler l’univers de la saga Fire Emblem développée par Intelligent Systems et édité par Nintendo.
