+++
title = "Roads of Anger"
date = 2023-10-14
updated = 2024-11-03

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1162747021342560327", author = "Lanoar", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1162747020759539803/Roads_of_Anger.pdf?ex=6728cf0c&is=67277d8c&hm=036ab439ab6853349b4e4f7ae0ebce35cd0c3679ca9be48926830fc783292c4a&", title = "Roads of Anger", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["Roads of Anger", "Streets of Rage", "jeux vidéos"]
authors = ["Lanoar"]

+++

Roads of Anger

Un cadre de jeu pour Anime Was a Mistake inspiré de Streets of Rage, la série de jeux vidéo publiée par SEGA puis Dotemu.

