+++
title = "Mahou Shoujo in the Darkness"
date = 2022-10-22
updated = 2022-10-22

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1032687298745221250", author = "Pierre", link = "https://docs.google.com/document/d/1oNYasobRy8y4JUBCLgmSrwlC_zzqZoCkUBLglfJXncw/edit?usp=sharing", title = "Mahou shoujo in the darkness", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Pierre “Poney” [La Minute Rôliste]"]

+++

Mahou Shoujo in the Darkness a été pensé pour émuler les univers de magical girls, que ce soit Sailormoon ou Puella Magi Madoka Magica, en passant par Card captor sakura ou encore Yojo Senki.
