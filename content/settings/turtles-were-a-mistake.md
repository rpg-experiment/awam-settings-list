+++
title = "Turtles Were A Mistake"
date = 2022-08-29
updated = 2022-08-29

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/586905829412700168/1013792284581564426", author = "Eolias", link = "https://cdn.discordapp.com/attachments/586905829412700168/1013792284204073063/TWAM.pdf", title = "Turtles Were A Mistake", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Eolias"]

+++

New York est connue pour ses gratte-ciels, sa statue de la Liberté et … ses pizzas. Ce que sa population ignore, c’est qu’une guerre secrète a lieu entre différents clans ninjas, organisations corporatistes, des extra-terrestres et même des animaux divins.

Quelle que soit votre origine, venez rejoindre ce combat ! Et n’oubliez pas de passer prendre ma commande chez Antonio’s Pizza !

Turtles Were A Mistake a été pensé pour émuler l'univers des torture ninjas.
