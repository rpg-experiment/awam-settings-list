+++
title = "Pré-tiré de Ronhin"
date = 2023-09-22
updated = 2024-11-03

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1154754309062398023", author = "Ronhin", title = "Pré-tiré de Ronhin", status = "Ready" }

[taxonomies]
categories = ["pré-tiré"]
tags = ["pré-tiré"]
authors = ["Ronhin"]

+++

Quelques pré-tirés issue de divers univers.

* [Mini Tachikawa (enfant elu)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754305224626196/AWAM_fiches_Mimi.png?ex=6728bc40&is=67276ac0&hm=90f60f2bd86d9d9f68aca267667fc5a8930d71c06a061b2313e3dcd1f8e960d3&)
* [Charmy Pappitson (chevalien-mage)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754305996361799/AWAM_fiches_Charmy.png?ex=6728bc40&is=67276ac0&hm=10458e998107a7f52c196495b61eece5ce0df9d2383d75ac4139109d5c05aa94&)
* [Itachi Uhiwa (Anbu)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754306445156502/AWAM_fiches_Itachi.png?ex=6728bc40&is=67276ac0&hm=eb83747d1dfc1e89249df6aca48822b9678211f6e3c730ac6d60083adfebfd41&)
* [Tony Tony Chopper (Renne pirate)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754307254669342/AWAM_fiche_Tony_Tony_Chopper.png?ex=6728bc41&is=67276ac1&hm=4d4102265e3b922767386825e01c00da8ba1b63d4b6007ea3a8b01facae661ff&)
* [Groob Kaboom (Judokoala)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754307820883978/AWAM_fiche_Groob.png?ex=6728bc41&is=67276ac1&hm=5790dc77fa23d8dea92d10b0a893093f927cbd983ee8fb999072222bde7489d1&)
* [Shikamaru Nara (Ninja des ombres)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754308286455889/AWAM_fiche_Shikamaru_Nara.png?ex=6728bc41&is=67276ac1&hm=54927c891fdeaf885336008ff04e0eb2559058e2582bee7b614c19c870edb533&)
* [Kakashi Hatake (Ninja Copieur)](https://cdn.discordapp.com/attachments/1032687098496561274/1154754308735258736/AWAM_fiche_Kakashi_Hatake.png?ex=6728bc41&is=67276ac1&hm=157fc45cc7f9363d646814e88a5264c9e179cd8c3c0bb6273900830b73038b0d&)
