+++
title = "Dragon Ball Sparking"
date = 2022-11-02
updated = 2023-03-29

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1037763835886575688", author = "Kaefer", link = "https://kaefer.itch.io/dragon-ball-sparking", title = "Dragon Ball Sparking ! Hack AWAM", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Kaefer"]

+++

Une adaptation de l’univers du manga d’Akira Toriyama pour le jeu de rôle Anime Was A Mistake de Quentin Forestier.

Cette adaptation non-officielle nécessite de posséder le jeu pour être utilisée, car elle n’apporte que les ajustements et les règles spécifiques pour émuler Dragon Ball. L’équilibrage est volontairement plus dirigé vers la première saga du manga, avec des niveaux de puissance plus bas et un gros plan sur l’aventure (la capacité de voler n’est pas automatique par exemple). Pour émuler la saga Z, n’hésitez pas à renforcer la puissance des atouts, inclure des capacités “gratuitement” ou à permettre de les cumuler avec le bento “no limit” !


