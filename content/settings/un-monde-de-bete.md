+++
title = "Un monde de bête"
date = 2022-10-23
updated = 2022-10-23

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1033733782487957635", author = "Spice", link = "https://docs.google.com/document/d/1XVWXpKSTxSn47THIv_iBqct_xae4_Bq1-EHMUYXU_Yk/edit?usp=sharing", title = "Un monde de bête", status = "Ready" }

[taxonomies]
categories = ["settings"]
authors = ["Spice"]

+++

Beastars Setting (Un monde de bête) a été pensé pour émuler l’univers du manga de Beastars créé par Paru Itagaki et édité en France par ki-oon
