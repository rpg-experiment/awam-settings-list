+++
title = "Bleach"
date = 2023-06-17
updated = 2023-06-21

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1121187952886808686", author = "Arkhanes", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1121187952530301058/Setting_Bleach_pour_AwaM_-_v_0.1.pdf", title = "Bleach", status = "Beta" }

[taxonomies]
categories = ["settings"]
tags = ["Bleach"]
authors = ["Arkhanes"]

+++

Proposition de setting pour Bleach. Considérez-le comme une version 0.0, en attente de commentaires bienveillants.

Lorsque j'aurai un setting définitif, je partagerai un lien Google Drive, comme d'autres avant moi.

Bonne lecture !
