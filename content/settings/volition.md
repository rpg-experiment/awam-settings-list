+++
title = "Volition"
date = 2023-04-12
updated = 2023-04-12

[extra]
awam = {discord_link = "https://discord.com/channels/423799120197844993/1032687098496561274/1095820309233401937", author = "Lanoar", link = "https://cdn.discordapp.com/attachments/1032687098496561274/1095820309090811954/volition-awam-v0_1.pdf", title = "Volition", status = "Ready" }

[taxonomies]
categories = ["settings"]
tags = ["volition", "SMT"]
authors = ["Lanoar"]

+++

Pour jouer dans un univers type SMT Persona (plus particulièrement le 5). On y joue des lycéens qui peuvent entrer et sortir librement dans un monde parallèle pour botter des culs (mais pas que). School life et bastons magiques au programme.
